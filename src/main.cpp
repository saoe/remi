/* =================================================================================================
Copyright © 2015-2016 Sascha Offe <so@saoe.net>.
Published under the terms of the MIT license <http://opensource.org/licenses/MIT>.
================================================================================================= */

#include <iostream>
#include <string>
#include <algorithm>

#include <Windows.h>
#include <Lm.h>
#include <WinCred.h>
#include <DSRole.h>
#include <tchar.h>

#include "version.h"


/* -------------------------------------------------------------------------------------------------
Checks if the string 'in' represents the switch 'key'.
Valid formats: {/|-}switch
'Key' can be handled case sensitive or case insensitive, depending on the function argument.
------------------------------------------------------------------------------------------------- */

bool isSwitch (const char* in, const char* key, bool case_insensitive = true)
{
	std::string in_str(in);
	std::string key_str(key);

	if (case_insensitive)
	{
		for (std::string::iterator i = in_str.begin()+1; i != in_str.end(); ++i)
			*i = ::toupper(*i);
		
		for (std::string::iterator j = key_str.begin(); j != key_str.end(); ++j)
		{
			*j = ::toupper(*j);
		}
	}

	std::string switch_value_v1 ("/" + key_str);
	std::string switch_value_v2 ("-" + key_str);
	
	if ((in_str.compare(switch_value_v1) == 0) || (in_str.compare(switch_value_v2) == 0))
		return true;
	else
		return false;
}



/* -------------------------------------------------------------------------------------------------
a.exe {/|-}switch {value|"long value with spaces"}

Returns true and the value if (and only if) it is preceded by switch.
------------------------------------------------------------------------------------------------- */

bool isSwitchAndValue (char* cmdline [], int i, const char* parent_switch, std::string* out, bool case_insensitive = true)
{
	if (isSwitch(cmdline[i], parent_switch, case_insensitive))
	{
		if (cmdline[i+1] != NULL && (cmdline[i+1][0] != '/' && cmdline[i+1][0] != '-'))
		{
			std::string retval(cmdline[i+1]);
			*out = retval;
			return true;
		}
	}
	
	return false;
}


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

int main (int argc, char* argv[])
{
	const int reboot_timeout_minimum = 0;
	const int reboot_timeout_maximum = 600;
	const int reboot_timeout_default = 60;
	int reboot_timeout = reboot_timeout_default;
	bool should_reboot = false;	
	
	wchar_t* domain = new wchar_t[16];

	// -------- Get command line arguments ------------------------------------------------------------------------

	if (argc == 1 || isSwitch(argv[1], "h") || isSwitch(argv[1], "help") || isSwitch(argv[1], "?"))
	{
		_tprintf(_T("\n\
%s %s - %s\n\
\n\
Usage: %s /c CurrentName /n NewName [/r [sec]]\n\
\n\
  /c <text>     Current name of the machine.\n\
  /n <text>     New name of the machine.\n\
  /r [seconds]  Reboot the computer to complete the process.\n\
                The default timeout is %i seconds.\n\
                The value can be in the range of %i-%i seconds.\n\
                A value of 0 initiates a immediate reboot without warning.\n\
\n\
Remarks:\n\
  (a) The executing user needs Account Operator rights in the domain.\n\
  (b) The reboot option requires the SE_REMOTE_SHUTDOWN_NAME privilege\n\
      to be enabled for the process that is calling this program\n\
      (i.e. call the program from an administrator's prompt).\n\
\n\
%s\n\
%s\n\
Visit %s for more information.\n"), APP_NAME, VERSION_STRING, APP_DESCRIPTION, APP_FILENAME, reboot_timeout_default, reboot_timeout_minimum, reboot_timeout_maximum, COPYRIGHT, APP_LICENSE, APP_HOMEPAGE);
	}
	else
	{
		std::cout << std::endl; // Empty line as visual separator for the following output.
		
		std::string cur_name;
		std::string new_name;

		// -------- Get command line arguments; skip the first (filename of the executable) --------
		for (int i = 1; i < argc; i++)
		{
			isSwitchAndValue(argv, i, "c", &cur_name);
			isSwitchAndValue(argv, i, "n", &new_name);
			
			if (isSwitch(argv[i], "r"))
			{
				should_reboot = true;

				if (argv[i+1] != NULL)
				{
					if (isdigit(*argv[i+1]))
					{
						reboot_timeout = atoi(argv[i+1]);

						if (reboot_timeout < reboot_timeout_minimum || reboot_timeout > reboot_timeout_maximum)
						{
							std::cout << "\n:: Failure :: Timeout for reboot out of range! Valid range: " << reboot_timeout_minimum << "-" << reboot_timeout_maximum << " seconds." << std::endl;
							return 1;
						}
					}
					else
					{
						std::cout << "\n:: Warning :: Timeout for reboot is not a number. The default value (" << reboot_timeout_default << " seconds) will be used." << std::endl;
					}
				}
			}
		}

		if (cur_name.empty() || new_name.empty())
		{
			std::cout << "\n:: Failure :: One of the parameters is empty:\n" << "Current name: " << cur_name << "\n" << "New name: " << new_name << std::endl;
			return 1;
		}

		// Convert names to UPPERCASE (just for easier comparison!).
		// MSDN on Computer Names <https://msdn.microsoft.com/de-de/library/windows/desktop/ms724220.aspx>
		std::string tmp_cur_name(cur_name);
		std::string tmp_new_name(new_name);

		std::transform(new_name.begin(), new_name.end(), tmp_new_name.begin(), ::toupper);
		std::transform(cur_name.begin(), cur_name.end(), tmp_cur_name.begin(), ::toupper);

		if (tmp_new_name == tmp_cur_name)
		{
			std::cout << "\n:: Failure :: Current and new name are identical." << std::endl;
			return 1;
		}
	
		// -------- Get the NetBIOS name of the Domain; will be later used for the credentials -----

		DSROLE_PRIMARY_DOMAIN_INFO_BASIC* dom_info;
		DWORD dom_status;

		dom_status = DsRoleGetPrimaryDomainInformation(NULL, DsRolePrimaryDomainInfoBasic, (PBYTE*)&dom_info);

		if (dom_status == ERROR_SUCCESS)
		{
			if (dom_info->DomainNameDns == NULL)
			{
				_tprintf(_T("\n:: Failure :: No domain.\n"));
				return 1;
			}
			else
			{
				domain = dom_info->DomainNameFlat;
			}
		}
		else
		{
			_tprintf(_T("\n:: Failure :: %S\n"), dom_status);
			return 1;
		}

		// -------- Get credentials from user ------------------------------------------------------

		BOOL  save = FALSE;
		DWORD cred_status;

		TCHAR username [CREDUI_MAX_USERNAME_LENGTH + 1]; // +1, because "CREDUI_MAX_USERNAME_LENGTH does not include the terminating null character."
		TCHAR password [CREDUI_MAX_PASSWORD_LENGTH + 1];

		SecureZeroMemory(username, sizeof(username));
		SecureZeroMemory(password, sizeof(password));

		cred_status = CredUICmdLinePromptForCredentials
		(
			domain,
			NULL,
			NULL,
			username,
			CREDUI_MAX_USERNAME_LENGTH + 1,
			password,
			CREDUI_MAX_PASSWORD_LENGTH + 1,
			&save,
			CREDUI_FLAGS_DO_NOT_PERSIST | CREDUI_FLAGS_EXCLUDE_CERTIFICATES
		);

		switch (cred_status)
		{
			case ERROR_INVALID_FLAGS:
				std::cout << "\n:: Failure :: ERROR_INVALID_FLAGS" << std::endl;
				return 1;
		
			case ERROR_INVALID_PARAMETER:
				std::cout << "\n:: Failure :: ERROR_INVALID_PARAMETER" << std::endl;
				return 1;
		
			case ERROR_NO_SUCH_LOGON_SESSION:
				std::cout << "\n:: Failure :: ERROR_NO_SUCH_LOGON_SESSION: The credential manager cannot be used." << std::endl;
				return 1;
		
			case NO_ERROR:
				break;
		
			default:
				_tprintf(_T("\n:: Warning :: CredUICmdLinePromptForCredentials(): Unknown return value: : 0x%x.\n"), cred_status);
				return 1;
		}


		// -------- Rename computer ----------------------------------------------------------------

		NET_API_STATUS rename_status;

		std::wstring widestr1 = std::wstring(cur_name.begin(), cur_name.end());
		wchar_t* converted_cur_name = const_cast<wchar_t*>(widestr1.c_str());

		std::wstring widestr2 = std::wstring(new_name.begin(), new_name.end());
		wchar_t* converted_new_name = const_cast<wchar_t*>(widestr2.c_str());

		rename_status = NetRenameMachineInDomain
		(
			converted_cur_name,
			converted_new_name,
			username,
			password,
			NETSETUP_ACCT_CREATE
		);

		SecureZeroMemory(username, sizeof(username));
		SecureZeroMemory(password, sizeof(password));

		switch (rename_status)
		{
			case NERR_Success:
				std::cout << "\n:: Success :: " << cur_name << " will be renamed to " << new_name << " after a reboot." << std::endl;
				break;
	
			case ERROR_ACCESS_DENIED:
				std::cout << "\n:: Failure :: ERROR_ACCESS_DENIED" << std::endl;
				return 1;

			case ERROR_INVALID_PARAMETER:
				std::cout << "\n:: Failure :: ERROR_INVALID_PARAMETER" << std::endl;
				return 1;

			case NERR_SetupNotJoined:
				std::cout << "\n:: Failure :: NERR_SETUPNOTJOINED: The computer is not currently joined to a domain." << std::endl;
				return 1;

			case NERR_SetupDomainController:
				std::cout << "\n:: Failure :: NERR_SETUPDOMAINCONTROLLER: The computer is a Domain Controller and cannot be unjoined from a domain." << std::endl;
				return 1;
			
			case ERROR_LOGON_FAILURE: // 0x52e (1326)
				std::cout << "\n:: Failure :: ERROR_LOGON_FAILURE: Most likely causes: The user name or password is incorrect or the account " << cur_name << " does not exist."<< std::endl;
				return 1;

			case ERROR_USER_EXISTS: // 0x... (1316)
			case NERR_UserExists:   // 0x8b0 (2224)
				std::cout << "\n:: Failure :: ERROR_USER_EXISTS or NERR_UserExists: The account " << new_name << " already exists." << std::endl;
				return 1;

			case ERROR_BAD_NETPATH: // 0x35 (53)
				std::cout << "\n:: Failure :: ERROR_BAD_NETPATH: Most likely causes: The computer is turned off or there is a problem with the network connectivity." << std::endl;
				return 1;

			case ERROR_NO_SUCH_DOMAIN: // 0x... (1335)
				std::cout << "\n:: Failure :: ERROR_NO_SUCH_DOMAIN: Failure to find or connect to a domain controller." << std::endl;
				return 1;

			case ERROR_TIME_SKEW: // 0x... (1398)
				std::cout << "\n:: Failure :: ERROR_TIME_SKEW: Time skew that can cause failure of Kerberos authentication." << std::endl;
				return 1;

			case ERROR_NO_LOGON_SERVERS: // 0x... (1311)
				std::cout << "\n:: Failure :: ERROR_NO_LOGON_SERVERS" << std::endl;
				return 1;

			case ERROR_DS_MACHINE_ACCOUNT_QUOTA_EXCEEDED: // 0x... (8557)
				std::cout << "\n:: Failure :: ERROR_DS_MACHINE_ACCOUNT_QUOTA_EXCEEDED" << std::endl;
				return 1;

			default:
				_tprintf(_T("\n:: Warning :: Unknown return value: 0x%x.\n"), rename_status);
		}

		
		// -------- Reboot computer (if requested) -------------------------------------------------

		if (should_reboot)
		{
			BOOL reboot_status = InitiateSystemShutdownEx
				(
					converted_cur_name,
					NULL, // The default text shows when the reboot will happen (but not why); if we provide our own text, only that message will appear.
					reboot_timeout, // Timeout in seconds (0 = immedidatly).
					TRUE, //  ForceAppsClosed?
					TRUE, // RebootAfterShutdown?
					SHTDN_REASON_MAJOR_OTHER | SHTDN_REASON_MINOR_OTHER | SHTDN_REASON_FLAG_PLANNED
				);

			if (reboot_status)
				std::cout << "\n:: Success :: " << cur_name << " should shutdown after " << reboot_timeout << " seconds and come back as " << new_name << std::endl;
			else
				std::cout << "\n:: Failure :: Command to restart " << cur_name << " failed!" << std::endl;
		}

	}

	return 0;
}
