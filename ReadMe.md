# Remi

Console program for Microsoft Windows to rename a remote computer in an Active Directory domain.  
(Similar to Microsoft's [netdom.exe renamecomputer](https://technet.microsoft.com/de-de/library/cc788029.aspx).)

Tested under Microsoft Windows XP and Microsoft Windows 7.

You can [**download**](https://bitbucket.org/saoe/remi/downloads/) a binary relase of the current version or try to build it yourself (see below).

## How to use

`remi.exe /c CurrentName /n NewName [/r [sec]]`

| Parameter     | Description                                               |
|---------------|-----------------------------------------------------------|
| /c _name_     | Current name of the machine.                              |
| /n _name_     | New name of the machine (will be converted to uppercase). |
| /r [_seconds_]| [_Optional_] Reboot the computer to complete the process.<br>The default timeout is 60 seconds.<br>The value can be in the range of 0-600 seconds.<br>A value of 0 initiates a immediate reboot without warning. |

**Remarks**

- The executing user needs Account Operator rights in the domain.
- The reboot option requires the SE_REMOTE_SHUTDOWN_NAME privilege to be enabled for the process that is calling this program (i.e. call the program from an administrator's prompt).


## How to build

See comments at the top of [CMakeLists.txt](src/CMakeLists.txt) for up-to-date instructions on how to build the project.

Requirements:

- Files from the [repository](https://bitbucket.org/saoe/remi)
- [CMake](http://www.cmake.org) (version 2.8 or higher)
- Microsoft Visual Studio 2017 with Windows 10 SDK

--------------------------------------------------------------------------------

Copyright © 2015-2018 [Sascha Offe](http://www.saoe.net/).  
Published under the [MIT license](https://opensource.org/licenses/MIT) (follow the link or look in to the source code's [License.txt](https://bitbucket.org/saoe/remi/src/tip/License.txt) for details).
